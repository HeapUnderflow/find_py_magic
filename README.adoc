= find_py_magic

This tool will attempt to find all files in the current directory containing python bytecode.
It does this by looking at the magic bytes.
It also attempts to discern the version of the bytecode.

This is super naive, and it will most likely not find anything that isnt *pure* bytecode.
(such as .exe wrappers)

== LICENSE

[source]
--
include::LICENSE[]
--