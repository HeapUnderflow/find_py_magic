use std::{fmt::Display, fs::File, io, io::Read, path::Path};

use io::Write;

const MAGIC_LEN: usize = 2;
const VERSION_LEN: usize = 83;
// Stolen from https://github.com/google/pytype/blob/d2208d8bd0bfa4b42a7db38166c94584009a5677/pytype/pyc/magic.py
const MAGICS: [([u8; MAGIC_LEN], (u8, u8)); VERSION_LEN] = [
	([0x99, 0x4e], (1, 5)),
	([0xfc, 0xc4], (1, 6)),
	([0x87, 0xc6], (2, 0)),
	([0x2a, 0xeb], (2, 1)),
	([0x2d, 0xed], (2, 2)),
	([0x3b, 0xf2], (2, 3)),
	([0x45, 0xf2], (2, 3)),
	([0x59, 0xf2], (2, 4)),
	([0x63, 0xf2], (2, 4)),
	([0x6d, 0xf2], (2, 4)),
	([0x77, 0xf2], (2, 5)),
	([0x81, 0xf2], (2, 5)),
	([0x8b, 0xf2], (2, 5)),
	([0x8c, 0xf2], (2, 5)),
	([0x95, 0xf2], (2, 5)),
	([0x9f, 0xf2], (2, 5)),
	([0xa9, 0xf2], (2, 5)),
	([0xb3, 0xf2], (2, 5)),
	([0xc7, 0xf2], (2, 6)),
	([0xd1, 0xf2], (2, 6)),
	([0xdb, 0xf2], (2, 7)),
	([0xe5, 0xf2], (2, 7)),
	([0xef, 0xf2], (2, 7)),
	([0xf9, 0xf2], (2, 7)),
	([0x03, 0xf3], (2, 7)),
	([0xb8, 0x0b], (3, 0)),
	([0xc2, 0x0b], (3, 0)),
	([0xcc, 0x0b], (3, 0)),
	([0xd6, 0x0b], (3, 0)),
	([0xe0, 0x0b], (3, 0)),
	([0xea, 0x0b], (3, 0)),
	([0xf4, 0x0b], (3, 0)),
	([0xf5, 0x0b], (3, 0)),
	([0xff, 0x0b], (3, 0)),
	([0x09, 0x0c], (3, 0)),
	([0x13, 0x0c], (3, 0)),
	([0x1d, 0x0c], (3, 0)),
	([0x1f, 0x0c], (3, 0)),
	([0x27, 0x0c], (3, 0)),
	([0x3b, 0x0c], (3, 0)),
	([0x45, 0x0c], (3, 1)),
	([0x4f, 0x0c], (3, 1)),
	([0x58, 0x0c], (3, 2)),
	([0x62, 0x0c], (3, 2)),
	([0x6c, 0x0c], (3, 2)),
	([0x76, 0x0c], (3, 3)),
	([0x80, 0x0c], (3, 3)),
	([0x94, 0x0c], (3, 3)),
	([0x9e, 0x0c], (3, 3)),
	([0xb2, 0x0c], (3, 4)),
	([0xbc, 0x0c], (3, 4)),
	([0xc6, 0x0c], (3, 4)),
	([0xd0, 0x0c], (3, 4)),
	([0xda, 0x0c], (3, 4)),
	([0xe4, 0x0c], (3, 4)),
	([0xee, 0x0c], (3, 4)),
	([0xf8, 0x0c], (3, 5)),
	([0x02, 0x0d], (3, 5)),
	([0x0c, 0x0d], (3, 5)),
	([0x16, 0x0d], (3, 5)),
	([0x17, 0x0d], (3, 5)),
	([0x20, 0x0d], (3, 6)),
	([0x21, 0x0d], (3, 6)),
	([0x2a, 0x0d], (3, 6)),
	([0x2b, 0x0d], (3, 6)),
	([0x2c, 0x0d], (3, 6)),
	([0x2d, 0x0d], (3, 6)),
	([0x2f, 0x0d], (3, 6)),
	([0x30, 0x0d], (3, 6)),
	([0x31, 0x0d], (3, 6)),
	([0x32, 0x0d], (3, 6)),
	([0x33, 0x0d], (3, 6)),
	([0x3e, 0x0d], (3, 7)),
	([0x3f, 0x0d], (3, 7)),
	([0x40, 0x0d], (3, 7)),
	([0x41, 0x0d], (3, 7)),
	([0x42, 0x0d], (3, 7)),
	([0x48, 0x0d], (3, 8)),
	([0x49, 0x0d], (3, 8)),
	([0x52, 0x0d], (3, 8)),
	([0x53, 0x0d], (3, 8)),
	([0x54, 0x0d], (3, 8)),
	([0x55, 0x0d], (3, 8)),
];

fn check_magic(p: impl AsRef<Path>) -> io::Result<Option<(u8, u8)>> {
	let mut buf = [0u8; MAGIC_LEN];

	if p.as_ref().metadata().map(|m| m.len() as usize).unwrap_or(0) < MAGIC_LEN {
		return Ok(None);
	}

	let mut f = File::open(p)?;
	f.read_exact(&mut buf)?;

	Ok(MAGICS
		.iter()
		.find(|(magic, _)| *magic == buf)
		.map(|(_, version)| *version))
}

#[inline]
fn flush_io() -> io::Result<()> { io::stdout().lock().flush() }

fn write_progress(c: usize, m: Option<impl Display>) -> io::Result<()> {
	if let Some(msg) = m {
		println!("{}", msg);
	}

	print!("\r[completed {}]", c);
	flush_io()?;

	Ok(())
}

fn main() -> io::Result<()> {
	for (c, item) in walkdir::WalkDir::new(".")
		.into_iter()
		.filter_map(|e| e.ok())
		.filter(|e| e.file_type().is_file())
		.enumerate()
	{
		let m = check_magic(item.path())?.map(|ver| {
			format!(
				"found python {}.{} magic in file {}",
				ver.0,
				ver.1,
				item.path().to_string_lossy()
			)
		});
		write_progress(c, m)?;
	}

	Ok(())
}
